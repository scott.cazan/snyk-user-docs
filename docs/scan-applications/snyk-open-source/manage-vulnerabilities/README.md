# Manage vulnerabilities

The documentation in this section explains the core process for fixing vulnerabilities and licensing issues in your Project.

Using Snyk helps you fix vulnerabilities. See [Upgrade package versions to fix vulnerabilities](upgrade-package-versions-to-fix-vulnerabilities.md) and [Snyk patches to fix vulnerabilities](snyk-patches-to-fix-vulnerabilities.md) for details.

To apply fixes, you can use different methods, including manual application using the Snyk Web UI and automatic pull requests opened by Snyk. See [Fix your vulnerabilities](fix-your-vulnerabilities.md) for more details.

For information on managing issues, see [Prioritizing and managing issues](../../../manage-issues/priorities-for-fixing-issues/) to understand how to decide which issues to fix.
