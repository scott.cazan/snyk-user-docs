# Snyk Open Source - supported languages and package managers

Snyk Open Source supports the following programming languages, build tools, and package managers.

{% hint style="info" %}
For environments supported with other Snyk products, see: [Snyk Code - Supported languages and frameworks](../../snyk-code/snyk-code-language-and-framework-support.md), [Snyk Container - Supported operating system distributions](../../snyk-container/how-snyk-container-works/supported-operating-system-distributions.md), and [Snyk IaC and Cloud - Supported providers](../../../scan-infrastructure/supported-iac-and-cloud-providers/).
{% endhint %}

| **Language**                                                                                                                           | **Package manager / build tool**                                                                    |
| -------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| [.NET](../../supported-languages-and-frameworks/.net/#open-source-and-licensing) (C#, F#, Visual Basic)                                | Nuget, Paket                                                                                        |
| [Bazel](../../../scan-application-code/snyk-open-source/snyk-open-source-supported-languages-and-package-managers/snyk-for-bazel.md)   | [API Test Dep Graph endpoint](https://snyk.docs.apiary.io/#reference/test/dep-graph/test-dep-graph) |
| [C / C++](../../supported-languages-and-frameworks/c-c++.md#open-source-and-licensing)                                                 | N/A                                                                                                 |
| [Elixir](../../../scan-application-code/snyk-open-source/snyk-open-source-supported-languages-and-package-managers/snyk-for-elixir.md) | hex                                                                                                 |
| [Go](../../../scan-application-code/snyk-open-source/snyk-open-source-supported-languages-and-package-managers/snyk-for-go.md)         | Go Modules, dep, govendor                                                                           |
| [Java and Kotlin](../../supported-languages-and-frameworks/java-and-kotlin.md#open-source-and-licensing)                               | Gradle, Maven                                                                                       |
| [JavaScript](../../supported-languages-and-frameworks/javascript.md#open-source-and-licensing)                                         | npm, yarn                                                                                           |
| [PHP](broken-reference)                                                                                                                | Composer                                                                                            |
| [Python](broken-reference)                                                                                                             | pip, Poetry, pipenv                                                                                 |
| [Ruby](broken-reference)                                                                                                               | Bundler                                                                                             |
| [Scala](broken-reference)                                                                                                              | sbt                                                                                                 |
| [Swift and Objective-C](../../supported-languages-and-frameworks/swift-and-objective-c.md#open-source-and-licensing)                   | CocoaPods, Swift Package Manager                                                                    |
