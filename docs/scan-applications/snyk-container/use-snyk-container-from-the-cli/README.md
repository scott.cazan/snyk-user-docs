# Use Snyk Container from the CLI

To use the CLI, ensure you [install](../../../snyk-cli/install-or-update-the-snyk-cli/) it and then [authenticate](../../../snyk-cli/commands/auth.md).

The Snyk Container Command Line Interface or [Snyk CLI](../../../snyk-cli/) helps you find and fix vulnerabilities in container images on your local machine.

To use Snyk Container from the CLI, see:

* [Scan and monitor images](scan-and-monitor-images.md)
* [Understand Snyk Container CLI results](../../../scan-containers/snyk-cli-for-container-security/understanding-snyk-container-cli-results.md)
* [Advanced use of Snyk Container CLI](../../../scan-containers/snyk-cli-for-container-security/advanced-snyk-container-cli-usage.md)

