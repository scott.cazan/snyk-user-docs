# Supported languages and frameworks

Understand the supported languages and frameworks to secure your software based on your environment.&#x20;

Snyk supports the following languages&#x20;

* [.NET](.net/)
* Apex
* [Bazel](../../scan-application-code/snyk-open-source/snyk-open-source-supported-languages-and-package-managers/snyk-for-bazel.md)
* [C/C++](c-c++.md)
* [Elixir](../../scan-application-code/snyk-open-source/snyk-open-source-supported-languages-and-package-managers/snyk-for-elixir.md)
* [Go](../../scan-application-code/snyk-open-source/snyk-open-source-supported-languages-and-package-managers/snyk-for-go.md)
* [Java and Kotlin](java-and-kotlin.md)
* [JavaScript](javascript.md)
* [PHP](php.md)
* [Python](python.md)&#x20;
* [Ruby](ruby.md)
* [Scala](scala.md)
* [Swift and Objective-C](swift-and-objective-c.md)





