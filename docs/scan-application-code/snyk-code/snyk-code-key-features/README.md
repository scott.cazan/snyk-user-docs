# Snyk Code key features: AI Engine, Web UI, supported integrations

Snyk Code key features include the following:

* [Snyk Code AI Engine](snyk-code-ai-engine.md)
* [Snyk Code Web UI](snyk-code-web-ui.md)
* [Snyk Code - Supported integrations](snyk-code-supported-integrations.md)
