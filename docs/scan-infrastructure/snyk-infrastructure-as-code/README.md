# Scan your IaC source code

Using Snyk IaC, you can:

* [Scan Terraform files](../../scan-cloud-configurations/snyk-infrastructure-as-code/scan-terraform-files/)
* [Scan CloudFormation files](../../scan-cloud-configurations/snyk-infrastructure-as-code/scan-cloudformation-files/)
* [Scan Kubernetes configuration files](../../scan-cloud-configurations/snyk-infrastructure-as-code/scan-kubernetes-configuration-files/)
* [Scan ARM configuration files](../../scan-cloud-configurations/snyk-infrastructure-as-code/scan-arm-configuration-files.md)
* [Scan Serverless files](../../scan-cloud-configurations/snyk-infrastructure-as-code/scan-serverless-files.md)

This section also has a list of [supported IaC and cloud providers](../supported-iac-and-cloud-providers/) and instructions for [disabling IaC scans per Organization](../../scan-cloud-configurations/snyk-infrastructure-as-code/disable-iac-scans.md).
