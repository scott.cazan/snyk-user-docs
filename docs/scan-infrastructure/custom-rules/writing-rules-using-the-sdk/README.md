# Writing rules using the SDK

To get you started with the SDK, you will learn how to:

1. [Parse a fixture file](../../../scan-cloud-configurations/snyk-infrastructure-as-code/custom-rules/getting-started-with-the-sdk/parsing-an-input-file.md), to help you understand how to write a rule.
2. [​Write a rule with the SDK](../../../scan-cloud-configurations/snyk-infrastructure-as-code/custom-rules/getting-started-with-the-sdk/writing-a-rule.md) using Rego.
3. [Add unit tests for the rules you have written](../../../scan-cloud-configurations/snyk-infrastructure-as-code/custom-rules/getting-started-with-the-sdk/testing-a-rule.md) to verify your rules.
4. [Build a bundle containing your custom rules](../../../scan-cloud-configurations/snyk-infrastructure-as-code/custom-rules/getting-started-with-the-sdk/bundling-rules.md) so that you can [use it with the Snyk CLI](../../../scan-cloud-configurations/snyk-infrastructure-as-code/custom-rules/use-iac-custom-rules-with-cli/).
5. [Push the bundle containing your custom rules to a container registry](../../../scan-cloud-configurations/snyk-infrastructure-as-code/custom-rules/getting-started-with-the-sdk/pushing-a-bundle.md) so that you can [enforce its usage with the Snyk CLI](../../../scan-cloud-configurations/snyk-infrastructure-as-code/custom-rules/use-iac-custom-rules-with-cli/).

<figure><img src="../../../.gitbook/assets/image (117) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (3).png" alt="Development to distribution workflow"><figcaption><p>Development to distribution workflow</p></figcaption></figure>
