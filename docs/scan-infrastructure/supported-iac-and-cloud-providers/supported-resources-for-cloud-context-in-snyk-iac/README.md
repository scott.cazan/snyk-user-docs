# Supported resources for cloud scans in Snyk IaC

For a list of supported Cloud resource types, see the documentation for each cloud provider listed:

* [AWS](../../snyk-iac+/supported-resources-for-cloud-context-in-snyk-iac/supported-aws-resources-for-cloud-context.md)
* [Azure](../../snyk-iac+/supported-resources-for-cloud-context-in-snyk-iac/supported-azure-resources-for-snyk-cloud-context.md)
* [Google](../../snyk-iac+/supported-resources-for-cloud-context-in-snyk-iac/supported-google-resources-for-snyk-cloud-context.md)
