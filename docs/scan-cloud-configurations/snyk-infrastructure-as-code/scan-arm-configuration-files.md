# Scan ARM configuration files

Snyk IaC currently supports scanning ARM configurations as follows:

* **IaC+**: All Snyk IaC workflows
* **Current IaC**: CLI only

For the CLI, users can share their ARM scan results with the platform and present them in the UI by using the [Share CLI results feature](snyk-cli-for-infrastructure-as-code/share-cli-results-with-the-snyk-web-ui.md).

For more information, see [Test your ARM files with the CLI tool](snyk-cli-for-infrastructure-as-code/test-your-iac-files/test-your-arm-files-with-snyk-cli.md).
