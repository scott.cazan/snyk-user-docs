# Snyk Preview

{% hint style="info" %}
[Snyk IDE plugins](../../integrations/ide-tools/) also have preview features. These preview features are separate from Snyk Preview and can be found in the documentation for the specific IDE plugin.
{% endhint %}

Snyk Preview allows you to get access to use new features that may not be available to all customers by default.

Users with Admin permissions can use the **Snyk Preview** option at the Organization and Group levels.

To enable a feature using Snyk Preview:

1. From either the Group or Organization level, select **Settings** > **Snyk Preview**:
2. Click **Enable feature preview** to enable or disable the feature you want to use or stop using.
3. Click **Save changes**.

<figure><img src="../../.gitbook/assets/Screenshot 2023-05-04 at 11.36.07.png" alt="Snyk Preview screen"><figcaption><p>Snyk Preview screen</p></figcaption></figure>

{% hint style="info" %}
After the feature is enabled at the Group level, all Organizations in the Group have this feature, and it cannot be disabled for individual Organizations in the Group.

Some features can be made available only at the Group level, as indicated for the features.
{% endhint %}
