# Enterprise setup

These pages are provided to help you understand, plan, and roll out your Enterprise-level Snyk solution.

## Guides for key activities

* [Getting started with the Snyk: Enterprise plan](getting-started-with-the-snyk-enterprise-plan.md)
* [Upgrading to the Enterprise plan](upgrading-to-the-enterprise-plan.md)
* [Preparing for implementation: Enterprise plan](preparing-for-implementation-of-the-enterprise-plan.md)

## How to authenticate and connect

* [Authenticate for third-party tools](authentication-for-third-party-tools.md)
* [Set up SSO](using-single-sign-on-sso-for-authentication/)
* [Snyk Broker](snyk-broker/)
* [Service accounts](service-accounts.md)
